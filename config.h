/* See LICENSE file for copyright and license details. */


/* appearance */
static const unsigned int borderpx  = 0;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
//static const unsigned int gappx     = 10; /*gAPS i like*/
static const unsigned int gappx     = 3;
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int user_bh = 27; /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const unsigned int systraypinning = 0;
static const unsigned int systrayspacing = 2;
static const int systraypinningfailfirst = 1;
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const int showsystray = 1;
static const char *fonts[]          = { "JetBrains Mono Nerd Font Mono:size=12" };
static const char dmenufont[]       = "JetBrains Mono Nerd Font Mono:size=11";
static char normbgcolor[]       = "#1E1E1E";
static char normbordercolor[]       = "#262626";
static char selfgcolor[]       = "#ffffff";
static char normfgcolor[]       = "#bababa";
static char selbordercolor[]        = "#1E1E1E";
static char selbgcolor[]        = "#1E1E1E";
static char *colors[][3] = {
	/*				 fg			  bg		   border	*/
	[SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
	[SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};

/* tagging */
static const char *tags[] = {
	" 爵 ", "  ", "  ", "  ", "  ", "  ", " 喇 ", "  ", "  "
};
static const char *tagsalt[] = {
	" 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ", " 9 "
};

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class		   instance  title			 tags mask	isfloating	isterminal	noswallow  monitor */
	{ "Gimp",		   NULL,	 NULL,			 0,			1,			0,			 0,		   -1 },
	{ "Firefox",	   NULL,	 NULL,			 1 << 8,	0,			0,			-1,		   -1 },
	{ "tabbed",			   NULL,	 NULL,			 0,			0,			1,			 0,		   -1 },
	{ NULL,			   NULL,	 "Event Tester", 0,			0,			0,			 1,		   -1 }, /* xev */
};

/* layout(s) */
static const float mfact	 = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster	 = 1;	 /* number of clients in master area */
static const int resizehints = 0;	 /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol	  arrange function */
	{ "[]=",	  tile },	 /* first entry is default */
	{ "><>",	  NULL },	 /* no layout function means floating behavior */
	{ "[M]",	  monocle },
	{ "|M|",	  centeredmaster },
	{ ">M>",	  centeredfloatingmaster },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,						KEY,	  view,			  {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,			KEY,	  toggleview,	  {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,				KEY,	  tag,			  {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,	  toggletag,	  {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/bash", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbordercolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]  = { "st", NULL }; /* Terminal (vitali64-st is recommended) */
static const char *showkeypresses[] = { "screenkey", NULL }; /* { OPTIONAL } screenkey */
static const char *layoutmenu_cmd = "layoutmenu.sh";
#define TERMINALRUN "st -e " /* Terminal to use for running terminal commands (vitali64-st is recommended) */
#define ST "st"
#define FILEMANAGER "pcmanfm" /* File manager */
#define BROWSER "torbrowser-launcher" /* Web browser */
#define WALLPAPERCHANGER "wal -n -i $(find /usr/share/backgrounds/ -type f -iregex '.*.\\(jpg\\|png\\)' | sort -R | head -1) --backend wal ; feh --bg-fill \"$(< \"${HOME}/.cache/wal/wal\" )\" " /* Change the wallpaper by using feh */
#define EMAILCLIENT "neomutt" /* E-Mail client */
#define SCRIPTS "~/scripts" /* { OPTIONAL } My scripts directory */
#define MEDIA_PLAYER "mpv --profile=pseudo-gui" /* The media player */
#define CHATCLIENT1 "irssi" /* {->IRC<-,XMPP,Matrix} client */
#define CHATCLIENT2 "nheko" /* {IRC,XMPP,->Matrix<-} client */

#include <X11/XF86keysym.h>

static Key keys[] = {
	/* modifier						key		   function		   argument */
	{ MODKEY,						XK_p,	   spawn,		   {.v = dmenucmd } },
	{ MODKEY,						XK_Return, spawn,		   SHCMD(ST) },
	{ MODKEY,						XK_b,	   togglebar,	   {0} },
	{ MODKEY,						XK_j,	   focusstack,	   {.i = +1 } },
	{ MODKEY,						XK_k,	   focusstack,	   {.i = -1 } },
	{ MODKEY,						XK_i,	   incnmaster,	   {.i = +1 } },
	{ MODKEY,						XK_d,	   incnmaster,	   {.i = -1 } },
	{ MODKEY,						XK_h,	   setmfact,	   {.f = -0.05} },
	{ MODKEY,						XK_l,	   setmfact,	   {.f = +0.05} },
	{ MODKEY|ShiftMask,				XK_Return, zoom,		   {0} },
	{ MODKEY,						XK_Tab,    view,		   {0} },
	{ MODKEY|ShiftMask,				XK_c,	   killclient,	   {0} },
	{ MODKEY,						XK_t,	   setlayout,	   {.v = &layouts[0]} },
	{ MODKEY,						XK_f,	   setlayout,	   {.v = &layouts[1]} },
	{ MODKEY,						XK_m,	   setlayout,	   {.v = &layouts[2]} },
	{ MODKEY,						XK_u,	   setlayout,	   {.v = &layouts[3]} },
	{ MODKEY,						XK_o,	   setlayout,	   {.v = &layouts[4]} },
	{ MODKEY,						XK_space,  setlayout,	   {0} },
	{ MODKEY|ShiftMask,				XK_space,  togglefloating, {0} },
	{ MODKEY,						XK_a,	   view,		   {.ui = ~0 } },
	{ MODKEY|ShiftMask,				XK_a,	   tag,			   {.ui = ~0 } },
	{ MODKEY,						XK_comma,  focusmon,	   {.i = -1 } },
	{ MODKEY,						XK_period, focusmon,	   {.i = +1 } },
	{ MODKEY|ShiftMask,				XK_comma,  tagmon,		   {.i = -1 } },
	{ MODKEY|ShiftMask,				XK_period, tagmon,		   {.i = +1 } },
	{ MODKEY|ShiftMask,				XK_f,	   togglefullscr,  {0} },
	{ MODKEY,						XK_n,	   togglealttag,   {0} },
	{ MODKEY,                       XK_F5,     xrdb,           {.v = NULL } },
	/* 
	 * Here goes our own bindings for running an app.
	 * For some bindings, you need to remove "TERMINALRUN"
	 * if the program is a GUI program 
	 * to prevent an unnecessary terminal to pop up, and to add
	 * "TERMINALRUN" if the program is a CLI program.
	 */
	{ MODKEY|ControlMask,			XK_Return, spawn,		   SHCMD(FILEMANAGER) },
	{ MODKEY|ControlMask,			XK_m,	   spawn,		   SHCMD(TERMINALRUN EMAILCLIENT) },
	{ MODKEY|ControlMask,			XK_space,  spawn,		   SHCMD(BROWSER) },
	{ MODKEY|ControlMask,			XK_Tab,    spawn,		   SHCMD(TERMINALRUN WALLPAPERCHANGER) },
	{ MODKEY|ControlMask,			XK_p,      spawn,		   SHCMD(MEDIA_PLAYER) },
	{ MODKEY|ControlMask,			XK_u,      spawn,		   SHCMD(TERMINALRUN CHATCLIENT1) },
	{ MODKEY|ControlMask,			XK_o,      spawn,		   SHCMD(CHATCLIENT2) },
	/* Other things */
	{ MODKEY|ShiftMask,				XK_h,	   setcfact,	   {.f = +0.25} },
	{ MODKEY|ShiftMask,				XK_l,	   setcfact,	   {.f = -0.25} },
	{ MODKEY|ShiftMask,				XK_o,	   setcfact,	   {.f =  0.00} },
	
	/* moveresize
	 * I most of the time don't use a mouse so I want a way to move windows
	 * with the keyboard, and my macbook's mouse has only one button so 
	 * I have to press with my 2 fingers and move at the same time which, 
	 * most of the time, doesn't work (this is registered as a scrolling movement)
	 * and is inconvinient.
	 */

	{ MODKEY,                       XK_Down,   moveresize,     {.v = "0x 25y 0w 0h" } },
	{ MODKEY,                       XK_Up,     moveresize,     {.v = "0x -25y 0w 0h" } },
	{ MODKEY,                       XK_Right,  moveresize,     {.v = "25x 0y 0w 0h" } },
	{ MODKEY,                       XK_Left,   moveresize,     {.v = "-25x 0y 0w 0h" } },
	{ MODKEY|ShiftMask,             XK_Down,   moveresize,     {.v = "0x 0y 0w 25h" } },
	{ MODKEY|ShiftMask,             XK_Up,     moveresize,     {.v = "0x 0y 0w -25h" } },
	{ MODKEY|ShiftMask,             XK_Right,  moveresize,     {.v = "0x 0y 25w 0h" } },
	{ MODKEY|ShiftMask,             XK_Left,   moveresize,     {.v = "0x 0y -25w 0h" } },
	{ MODKEY|ControlMask,           XK_Up,     moveresizeedge, {.v = "t"} },
	{ MODKEY|ControlMask,           XK_Down,   moveresizeedge, {.v = "b"} },
	{ MODKEY|ControlMask,           XK_Left,   moveresizeedge, {.v = "l"} },
	{ MODKEY|ControlMask,           XK_Right,  moveresizeedge, {.v = "r"} },
	{ MODKEY|ControlMask|ShiftMask, XK_Up,     moveresizeedge, {.v = "T"} },
	{ MODKEY|ControlMask|ShiftMask, XK_Down,   moveresizeedge, {.v = "B"} },
	{ MODKEY|ControlMask|ShiftMask, XK_Left,   moveresizeedge, {.v = "L"} },
	{ MODKEY|ControlMask|ShiftMask, XK_Right,  moveresizeedge, {.v = "R"} },

	/* Here are buttons to, for example, lower the volume, 
	 * the brightness, etc...
	 */

	{0,	XF86XK_AudioRaiseVolume,			   spawn,		   SHCMD("amixer set Master 5%+; pkill -RTMIN+10 dwmblocks") },
	{0,	XF86XK_AudioLowerVolume,		       spawn,		   SHCMD("amixer set Master 5%-; pkill -RTMIN+10 dwmblocks") },
	{0,	XF86XK_MonBrightnessUp,			       spawn,		   SHCMD("xbacklight -dec -10; pkill -RTMIN+15 dwmblocks") },
	{0,	XF86XK_MonBrightnessDown,		       spawn,		   SHCMD("xbacklight -dec 10; pkill -RTMIN+15 dwmblocks") },
	{0,	XF86XK_AudioMute,				       spawn,		   SHCMD("amixer set Master toggle") },
	{0,	XF86XK_Eject,					       spawn,		   SHCMD("slock") },


	TAGKEYS(						XK_1,					   0)
	TAGKEYS(						XK_2,					   1)
	TAGKEYS(						XK_3,					   2)
	TAGKEYS(						XK_4,					   3)
	TAGKEYS(						XK_5,					   4)
	TAGKEYS(						XK_6,					   5)
	TAGKEYS(						XK_7,					   6)
	TAGKEYS(						XK_8,					   7)
	TAGKEYS(						XK_9,					   8)
	{ MODKEY|ShiftMask,				XK_q,	   quit,		   {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click				event mask		button			function		argument */
	{ ClkLtSymbol,			0,				Button1,		setlayout,		{0} },
	/*{ ClkLtSymbol,		  0,			  Button3,		  setlayout,	  {.v = &layouts[2]} },*/
	{ ClkLtSymbol,			0,				Button3,		layoutmenu,		{0} },
	{ ClkWinTitle,			0,				Button2,		zoom,			{0} },
	{ ClkStatusText,		0,				Button2,		spawn,			{.v = termcmd } },
	{ ClkClientWin,			MODKEY,			Button1,		movemouse,		{0} },
	{ ClkClientWin,			MODKEY,			Button2,		togglefloating, {0} },
	{ ClkClientWin,			MODKEY,			Button3,		resizemouse,	{0} },
	{ ClkTagBar,			0,				Button1,		view,			{0} },
	{ ClkTagBar,			0,				Button3,		toggleview,		{0} },
	{ ClkTagBar,			MODKEY,			Button1,		tag,			{0} },
	{ ClkTagBar,			MODKEY,			Button3,		toggletag,		{0} },
};

