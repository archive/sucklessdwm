<div style="background: red;">This is unmaintained. I'm now using Wayland</div>

# v64-dwm - Vitali64's dynamic window manager build
dwm is an extremely fast, small, and dynamic window manager for X.

<img src="http://vitali64.duckdns.org/?p=suckless/dwm.git;a=blob_plain;f=Screenshot.png;hb=HEAD"></img>

*dwmblocks and wallpaper AREN'T included*

# My build

My build adds a lot of patches. The colors also adapt to the wallpaper if 
pywal is installed, otherwise, it will use the default (Mod+F5).

# Requirements

In order to build dwm you need the Xlib header files, GNU+Linux or BSD (FreeBSD, OpenBSD, ...) with Xorg installed.
It doesn't and will not work on other systems such as macOS and Windows because this relies on libraries available exclusively on Linux.

This has been tested on Artix Linux but should work with any other distro such as Ubuntu Linux

Installation
------------
Edit `config.mk` to match your local setup (most of the time, the `config.mk` provided here should work without any modifications).

Afterwards enter the following command to build (if
necessary as root):

    make

And if you want to install it on the system, run the following :

    sudo make clean install

Running dwm
-----------
Add the following line to your .xinitrc to start dwm using startx:

    exec <where the binary file is located>/dwm

In order to connect dwm to a specific display, make sure that
the DISPLAY environment variable is set correctly, e.g.:

    DISPLAY=foobar:1 exec dwm

(This will start dwm on display :1 of the host foo.bar.)

In order to display status info in the bar, you can do something
like this in your .xinitrc:

    while xsetroot -name "`date` `uptime | sed 's/.*,//'`"
    do
    	sleep 1
    done &
    exec dwm

You can also use dwmblocks for that.

